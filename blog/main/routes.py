from flask import render_template, request, Blueprint
from flask_login import current_user
from blog.models import Post

main = Blueprint('main', __name__)


@main.route("/")
@main.route("/home")
def home():
    if current_user.is_authenticated:
        page = request.args.get('page', 1, type=int)
        posts = Post.query.order_by(Post.pub_date.desc()).paginate(page=page, per_page=10)
        return render_template('home.html', posts=posts)
    return render_template('home.html')
